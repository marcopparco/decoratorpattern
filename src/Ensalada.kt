
interface Ensalada{
    fun getIngredientes()
}

class ConcreteEnsalada: Ensalada{
    override fun getIngredientes() {
        println("Esto es Crema de Chantilly")
    }
}

open class EnsaladaDecorator(protected var ensalada:Ensalada): Ensalada{
    override fun getIngredientes() {
        this.ensalada.getIngredientes()
    }
}


class VainillaEnsalada(m:Ensalada): EnsaladaDecorator(m){
    override fun getIngredientes() {
        super.getIngredientes()
        this.addIngredientes()
        println("Esto es una Ensalada de Frutas con crema de vainilla")
    }
    public fun addIngredientes(){
        println("Agregando sabor a vainilla a la ensalada")
    }
}


public class GalletasDeChocolateEnsalada(m:Ensalada): EnsaladaDecorator(m){
    override fun getIngredientes() {
        super.getIngredientes()
        this.addIngredientes()
        println("Esto es Galletas de Chocolate")
    }

    public fun addIngredientes(){
        println("Agregando Galletas de Chocolate a la ensalada")
    }
}


fun main(args: Array<String>){
    val galletasDeChocolateEnsalada= GalletasDeChocolateEnsalada(ConcreteEnsalada())
    galletasDeChocolateEnsalada.getIngredientes()
    val vainillaEnsalada=VainillaEnsalada(ConcreteEnsalada())
    vainillaEnsalada.getIngredientes()
}